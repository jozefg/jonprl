Operator RawCat : ().
Operator obj : (0).
Operator hom : (0;0;0).
Operator idn : (0;0).
Operator cmp : (0;0;0;0;0;0).

[RawCat] =def= [
  (Obj:U{i})
  * (Hom : Obj -> Obj -> U{i})
  * (Idn : (A:Obj) Hom A A)
  * (Cmp : (A:Obj) (B:Obj) (C:Obj) Hom B C -> Hom A B -> Hom A C)
  * unit
].

[obj(C)] =def= [spread(C; x.y.x)].
[hom(C;A;B)] =def= [spread(spread(C; x.y.y); x.y.x) A B].
[idn(C;A)] =def= [spread(spread(spread(C; x.y.y); x.y.y); x.y.x) A].
[cmp(C;X;Y;Z;f;g)] =def= [spread(spread(spread(spread(C; x.y.y); x.y.y); x.y.y); x.y.x) X Y Z f g].

Tactic rawcat-unfold {
  unfold <RawCat obj hom idn cmp unit>.
}.

Tactic autoR {
  reduce; *{auto; reduce}.
}.

Theorem RawCat-wf : [member(RawCat; U{i'})] {
  rawcat-unfold; auto.
}.

Resource wf += { wf-lemma <RawCat-wf> }.

Resource intro += {
  @{ [|- {RC : RawCat} P] => intro @i'
   | [|- {A : obj(C)} P] => intro @i
   }
}.

Theorem obj-wf : [{RC:RawCat} member(obj(RC); U{i})] {
  auto; unfold <obj RawCat>; auto
}.

Resource wf += { wf-lemma <obj-wf> }.

Theorem hom-wf : [
  {RC:RawCat} {A:obj(RC)} {B:obj(RC)} member(hom(RC;A;B); U{i})
] {
  auto;
  unfold <RawCat hom obj>;
  auto; reduce; auto
}.

Resource wf += { wf-lemma <hom-wf> }.

Theorem idn-wf : [
  {RC:RawCat} {A:obj(RC)} member(idn(RC; A); hom(RC; A; A))
] {
  auto; unfold <RawCat idn obj hom>;
  auto; reduce; auto
}.

Resource wf += { wf-lemma <idn-wf> }.

Theorem cmp-wf : [
  {RC:RawCat}
  {X:obj(RC)}
  {Y:obj(RC)}
  {Z:obj(RC)}
  {f:hom(RC; Y; Z)}
  {g:hom(RC; X; Y)}
    member(cmp(RC;X;Y;Z;f;g); hom(RC; X;Z))
] {
  auto; unfold <RawCat cmp hom obj>;
  auto; reduce; auto
}.

Resource wf += { wf-lemma <cmp-wf> }.

Operator LeftIdentity : (0).
[LeftIdentity(C)] =def= [{A:obj(C)} {B:obj(C)} {f:hom(C;A;B)} =(cmp(C;A;B;B; idn(C;B); f); f; hom(C;A;B))].

Operator RightIdentity : (0).
[RightIdentity(C)] =def= [{A:obj(C)} {B:obj(C)} {f:hom(C;A;B)} =(cmp(C;A;A;B; f; idn(C;A)); f; hom(C;A;B))].

Operator CmpAssoc : (0).
[CmpAssoc(C)] =def= [
  {W:obj(C)} {X:obj(C)} {Y:obj(C)} {Z:obj(C)}
  {f:hom(C;W;X)}
  {g:hom(C;X;Y)}
  {h:hom(C;Y;Z)}
    =(cmp(C;W;X;Z; cmp(C;X;Y;Z; h;g); f);
      cmp(C;W;Y;Z; h; cmp(C;W;X;Y; g;f));
      hom(C;W;Z))
].

Theorem LeftIdentity-wf : [{RC:RawCat} member(LeftIdentity(RC); U{i})] {
  unfold <LeftIdentity>; auto;
}.

Theorem RightIdentity-wf : [{RC:RawCat} member(RightIdentity(RC); U{i})] {
  unfold <RightIdentity>; auto
}.

Theorem CmpAssoc-wf : [{RC:RawCat} member(CmpAssoc(RC); U{i})] {
  unfold <CmpAssoc>; auto
}.

Operator LawCat : (0).
[LawCat(RC)] =def= [LeftIdentity(RC) * RightIdentity(RC) * CmpAssoc(RC)].

Tactic lawcat-unfold {
  unfold <LeftIdentity RightIdentity CmpAssoc LawCat>;
  rawcat-unfold.
}.

Resource wf += { wf-lemma <LeftIdentity-wf> }.
Resource wf += { wf-lemma <RightIdentity-wf> }.
Resource wf += { wf-lemma <CmpAssoc-wf> }.

Theorem LawCat-wf : [{RC:RawCat} member(LawCat(RC); U{i})] {
  unfold <LawCat>; auto
}.

Resource wf += { wf-lemma <LawCat-wf> }.

Operator Cat : ().
[Cat] =def= [{C : RawCat | LawCat(C)}].

Tactic cat-unfold {
  unfold <Cat>; lawcat-unfold.
}.

Theorem Cat-wf : [member(Cat; U{i'})] {
  unfold <Cat>; auto; cum @i; auto
}.

Theorem InitialRawCat : [RawCat] {
  unfold <RawCat>;
  intro [void]; auto;
  intro [lam(A. lam(B. void))];
  auto
}.

Theorem InitialCat : [Cat] {
  unfold <Cat>;
  intro [InitialRawCat] @i;
  unfold <InitialRawCat>;
  [id, id, auto; elim #2 [C]; auto];

  |||TODO: we need a rule that says "this is the extract of this lemma, and is thence well-formed"
  *{lawcat-unfold; auto; reduce}
}.

Theorem TerminalRawCat : [RawCat] {
  unfold <RawCat>;
  intro [unit]; autoR;
  intro [lam(A. lam(B. unit))]; autoR
}.

Tactic approx-eta {
  @{ [f : approx(_;_) |- =(f; <>; approx(_;_))] => unfold <unit>; elim <f>; eq-cd }
}.

Theorem TerminalCat : [Cat] {
  unfold <Cat>;
  intro [TerminalRawCat] <C> @i; unfold <TerminalRawCat>; auto;

  *{lawcat-unfold; auto; reduce};
  symmetry;
  elim #1; approx-eta; auto
}.
